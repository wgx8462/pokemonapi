package com.gb.pokemonapi.controller;

import com.gb.pokemonapi.entity.Pokemon;
import com.gb.pokemonapi.model.CommonResult;
import com.gb.pokemonapi.model.SingleResult;
import com.gb.pokemonapi.model.dual.DualInPokemonResponse;
import com.gb.pokemonapi.service.DualService;
import com.gb.pokemonapi.service.PokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/dual")
public class DualController {
    private final PokemonService pokemonService;
    private final DualService dualService;

    @PostMapping("/pokemon-id/{pokemonId}/join")
    public CommonResult setDual(@PathVariable long pokemonId) throws Exception {
        Pokemon pokemon = pokemonService.getData(pokemonId);
        dualService.setDual(pokemon);

        CommonResult result = new CommonResult();
        result.setMsg("입장 성공");
        result.setCode(0);

        return result;
    }

    @DeleteMapping("/dual-id/{dualId}")
    public CommonResult delPokemon(@PathVariable long dualId) {
        dualService.delPokemon(dualId);

        CommonResult result = new CommonResult();
        result.setMsg("퇴장 완료");
        result.setCode(0);

        return result;
    }

    @GetMapping("/dual-stage/current")
    public SingleResult<DualInPokemonResponse> getCurrentState() {
        DualInPokemonResponse result = dualService.getCurrentState();

        SingleResult<DualInPokemonResponse> response = new SingleResult<>();
        response.setData(result);
        response.setMsg("입장");
        response.setCode(0);

        return response;
    }
}
