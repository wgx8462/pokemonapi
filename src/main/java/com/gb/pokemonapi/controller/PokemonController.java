package com.gb.pokemonapi.controller;

import com.gb.pokemonapi.model.*;
import com.gb.pokemonapi.model.pokemon.PokemonCreateRequest;
import com.gb.pokemonapi.model.pokemon.PokemonItem;
import com.gb.pokemonapi.model.pokemon.PokemonResponse;
import com.gb.pokemonapi.service.PokemonService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/pokemon")
public class PokemonController {
    private final PokemonService pokemonService;

    @PostMapping("/join")
    public CommonResult setPokemon(@RequestBody PokemonCreateRequest request) {
        pokemonService.setPokemon(request);

        CommonResult result = new CommonResult();
        result.setMsg("등록 완료 되었습니다.");
        result.setCode(0);

        return result;
    }

    @GetMapping("/all")
    public ListResult<PokemonItem> getPokemons() {
        List<PokemonItem> list = pokemonService.getPokemons();

        ListResult<PokemonItem> response = new ListResult<>();
        response.setList(list);
        response.setTotalCount(list.size());
        response.setMsg("성공하였습니다.");
        response.setCode(0);

        return response;
    }

    @GetMapping("/detail/{id}")
    public SingleResult<PokemonResponse> getPokemon(@PathVariable long id) {
        PokemonResponse result = pokemonService.getPokemon(id);

        SingleResult<PokemonResponse> response = new SingleResult<>();
        response.setData(result);
        response.setMsg("성공하였습니다.");
        response.setCode(0);

        return response;
    }
}
