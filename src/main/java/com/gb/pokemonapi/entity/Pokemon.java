package com.gb.pokemonapi.entity;

import com.gb.pokemonapi.enums.Type;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Pokemon {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String imageUrl;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private Type type;

    @Column(nullable = false)
    private String characteristic;

    @Column(nullable = false)
    private String classification;

    @Column(nullable = false)
    private String explanation;

    @Column(nullable = false)
    private Integer vitality;

    @Column(nullable = false)
    private Integer power;

    @Column(nullable = false)
    private Integer defensive;

    @Column(nullable = false)
    private Integer speed;
}
