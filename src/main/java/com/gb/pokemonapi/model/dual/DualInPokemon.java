package com.gb.pokemonapi.model.dual;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DualInPokemon {
    private Long pokemonDualId;
    private Long pokemonId;
    private String imageUrl;
    private String name;
    private Integer vitality;
    private Integer power;
    private Integer defensive;
    private Integer speed;
}
