package com.gb.pokemonapi.model.dual;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DualInPokemonResponse {
    private DualInPokemon pokemon1;
    private DualInPokemon pokemon2;
}
