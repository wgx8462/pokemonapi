package com.gb.pokemonapi.model.pokemon;

import com.gb.pokemonapi.enums.Type;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PokemonCreateRequest {
    private String imageUrl;
    private String name;
    @Enumerated(value = EnumType.STRING)
    private Type type;
    private String characteristic;
    private String classification;
    private String explanation;
    private Integer vitality;
    private Integer power;
    private Integer defensive;
    private Integer speed;
}
