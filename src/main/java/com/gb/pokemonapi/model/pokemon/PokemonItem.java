package com.gb.pokemonapi.model.pokemon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PokemonItem {
    private Long id;
    private String imageUrl;
    private String name;
    private String type;
}
