package com.gb.pokemonapi.model.pokemon;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PokemonResponse {
    private Long id;
    private String imageUrl;
    private String name;
    private String type;
    private String characteristic;
    private String classification;
    private String explanation;
    private Integer vitality;
    private Integer power;
    private Integer defensive;
    private Integer speed;
}
