package com.gb.pokemonapi.repository;

import com.gb.pokemonapi.entity.Dual;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DualRepository extends JpaRepository<Dual, Long> {
}
