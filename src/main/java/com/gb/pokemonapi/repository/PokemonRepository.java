package com.gb.pokemonapi.repository;

import com.gb.pokemonapi.entity.Pokemon;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PokemonRepository extends JpaRepository<Pokemon, Long> {
    List<Pokemon> findAllByIdGreaterThanEqualOrderByIdDesc(long id);
}
