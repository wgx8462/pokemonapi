package com.gb.pokemonapi.service;

import com.gb.pokemonapi.entity.Dual;
import com.gb.pokemonapi.entity.Pokemon;
import com.gb.pokemonapi.model.dual.DualInPokemon;
import com.gb.pokemonapi.model.dual.DualInPokemonResponse;
import com.gb.pokemonapi.repository.DualRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor // 종속성 자동주입
public class DualService {
    private final DualRepository dualRepository;

    public void setDual(Pokemon pokemon) throws Exception {
        // 몬스터 둘 이상 결투장 진입이 불가능 하니까.. 현재 몇놈의 몬스터가 결투장에 있는지 알아야 한다.
        // 결투장에 진입한 몬스터 리스트를 가져온다.
        List<Dual> checkList = dualRepository.findAll();
        // 만약 .... checkList의 갯수가 2개 2이상이면 던지자..
        if (checkList.size() >= 2) throw new Exception();

        Dual addData = new Dual();
        addData.setPokemon(pokemon);

        dualRepository.save(addData);
    }

    public void delPokemon(long id) {
        dualRepository.deleteById(id);
    }

    public DualInPokemonResponse getCurrentState() {
        // 일단.. 결투장에 진입한 몬스터 리스트 다 가져와.. 근데 넣을때 최대 2마리만 넣을 수있게 해놨으니
        // 경우의 수는 0개, 1개, 2개
        List<Dual> checkList = dualRepository.findAll();
        //DualInPokemonResponse 모양으로 무조건 줘야한다.
        DualInPokemonResponse response = new DualInPokemonResponse();
        // 이렇게 하면 안쪽에 칸들은 두칸다 null
        // 두칸 다 null 이란건 0개 일때 이미 처리하고 간다는거.

        if (checkList.size() == 2) {
            response.setPokemon1(convertPokemonItem(checkList.get(0)));
            response.setPokemon2(convertPokemonItem(checkList.get(1)));
        } else if (checkList.size() == 1) {
            response.setPokemon1(convertPokemonItem(checkList.get(0)));
        }
        return response;
    }

    private DualInPokemon convertPokemonItem(Dual dual) {
        DualInPokemon pokemonItem = new DualInPokemon();
        pokemonItem.setPokemonDualId(dual.getId());
        pokemonItem.setPokemonId(dual.getPokemon().getId());
        pokemonItem.setImageUrl(dual.getPokemon().getImageUrl());
        pokemonItem.setName(dual.getPokemon().getName());
        pokemonItem.setVitality(dual.getPokemon().getVitality());
        pokemonItem.setPower(dual.getPokemon().getPower());
        pokemonItem.setDefensive(dual.getPokemon().getDefensive());
        pokemonItem.setSpeed(dual.getPokemon().getSpeed());

        return pokemonItem;

    }
}
