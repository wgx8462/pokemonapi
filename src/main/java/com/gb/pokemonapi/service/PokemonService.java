package com.gb.pokemonapi.service;

import com.gb.pokemonapi.entity.Pokemon;
import com.gb.pokemonapi.model.pokemon.PokemonCreateRequest;
import com.gb.pokemonapi.model.pokemon.PokemonItem;
import com.gb.pokemonapi.model.pokemon.PokemonResponse;
import com.gb.pokemonapi.repository.PokemonRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class PokemonService {
    private final PokemonRepository pokemonRepository;

    public Pokemon getData(long id) {
        return pokemonRepository.findById(id).orElseThrow();
    }

    public void setPokemon(PokemonCreateRequest request) {
        Pokemon addData = new Pokemon();
        addData.setImageUrl(request.getImageUrl());
        addData.setName(request.getName());
        addData.setType(request.getType());
        addData.setCharacteristic(request.getCharacteristic());
        addData.setClassification(request.getClassification());
        addData.setExplanation(request.getExplanation());
        addData.setVitality(request.getVitality());
        addData.setPower(request.getPower());
        addData.setDefensive(request.getDefensive());
        addData.setSpeed(request.getSpeed());

        pokemonRepository.save(addData);
    }

    public List<PokemonItem> getPokemons() {
        List<Pokemon> originData = pokemonRepository.findAllByIdGreaterThanEqualOrderByIdDesc(1);
        List<PokemonItem> result = new LinkedList<>();

        for (Pokemon pokemon : originData) {
            PokemonItem addItem = new PokemonItem();
            addItem.setId(pokemon.getId());
            addItem.setImageUrl(pokemon.getImageUrl());
            addItem.setName(pokemon.getName());
            addItem.setType(pokemon.getType().getName());
            result.add(addItem);
        }
        return result;
    }

    public PokemonResponse getPokemon(long id) {
        Pokemon originData = pokemonRepository.findById(id).orElseThrow();
        PokemonResponse response = new PokemonResponse();

        response.setId(originData.getId());
        response.setImageUrl(originData.getImageUrl());
        response.setName(originData.getName());
        response.setType(originData.getType().getName());
        response.setCharacteristic(originData.getCharacteristic());
        response.setClassification(originData.getClassification());
        response.setExplanation(originData.getExplanation());
        response.setVitality(originData.getVitality());
        response.setPower(originData.getPower());
        response.setDefensive(originData.getDefensive());
        response.setSpeed(originData.getSpeed());

        return response;
    }
}
